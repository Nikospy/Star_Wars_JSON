package com.mikolaj.pietkiewicz.starwars;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class StarWarsMappingTest {
    public static void main(String[] args) throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException, UnirestException {
        setupUnirest();

        System.out.println("Just pick a number and enter it");
        int number = new Scanner(System.in).nextInt();

        StarWarsCharacterInfo starWarsCharacterInfo = Unirest
                .get("https://swapi.co/api/people/"+number)
                .asObject(StarWarsCharacterInfo.class)
                .getBody();

        String numOfCurrCharacterHomeworld = starWarsCharacterInfo.getHomeworld();
        numOfCurrCharacterHomeworld = String.valueOf(numOfCurrCharacterHomeworld.charAt(numOfCurrCharacterHomeworld.length()-2));

        String[] vehicle = new String[starWarsCharacterInfo.getVehicles().size()];
        String[] vehicleNumber = new String[starWarsCharacterInfo.getVehicles().size()];
        for (int i = 0; i < vehicle.length; i++) {
            vehicle[i] = starWarsCharacterInfo.getVehicles().get(i);
            String[] tokens = vehicle[i].split("/");
            vehicleNumber[i] = tokens[tokens.length-1];
        }

        HomeWorld homeWorld  = Unirest
                .get("https://swapi.co/api/planets/"+numOfCurrCharacterHomeworld)
                .asObject(HomeWorld.class)
                .getBody();


        System.out.println(starWarsCharacterInfo);
        System.out.println(homeWorld);

        for (int i = 0; i < vehicle.length; i++) {
            Vehicles vehicles = Unirest
                    .get("https://swapi.co/api/vehicles/" + vehicleNumber[i])
                    .asObject(Vehicles.class)
                    .getBody();
            System.out.println(vehicles);
        }

    }

    private static void setupUnirest() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        Unirest.setObjectMapper(new ObjectMapper() {
            private com.fasterxml.jackson.databind.ObjectMapper jacksonObjectMapper
                    = new com.fasterxml.jackson.databind.ObjectMapper();

            public <T> T readValue(String value, Class<T> valueType) {
                try {
                    return jacksonObjectMapper.readValue(value, valueType);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            public String writeValue(Object value) {
                try {
                    return jacksonObjectMapper.writeValueAsString(value);
                } catch (JsonProcessingException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }
}

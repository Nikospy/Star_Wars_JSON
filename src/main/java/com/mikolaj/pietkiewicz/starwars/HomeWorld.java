package com.mikolaj.pietkiewicz.starwars;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class HomeWorld {
    private String name;
    private String climate;
    private String gravity;
    private String terrain;
    private String surface_water;
    private String population;

    @Override
    public String toString() {
        return "\n*** Home world ***" +
                "\nName: " + name +
                "\nClimate: " + climate +
                "\nGravity: " + gravity +
                "\nTerrain: " + terrain +
                "\nSurface_water: " + surface_water +
                "\nPopulation: " + population+
                "\n";
    }
}

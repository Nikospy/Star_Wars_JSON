package com.mikolaj.pietkiewicz.starwars;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Vehicles {
    private String name;
    private String model;
    private String manufacturer;
    private String cost_in_credits;
    private String length;
    private String max_atmosphering_speed;
    private String crew;
    private String passengers;
    private String consumables;
    private String vehicle_class;
    private List<String> pilots;

    @Override
    public String toString() {
        return "*** Vehicles ***" +
                "\nName: " + name +
                "\nModel: " + model +
                "\nManufacturer: " + manufacturer +
                "\nCost_in_credits: " + cost_in_credits +
                "\nLength: " + length +
                "\nMax_atmosphering_speed: " + max_atmosphering_speed +
                "\nCrew: " + crew +
                "\nPassengers: " + passengers +
                "\nConsumables: " + consumables +
                "\nVehicle_class: " + vehicle_class +
                "\nPilots: " + pilots+
                "\n";
    }
}

package com.mikolaj.pietkiewicz.starwars;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class StarWarsCharacterInfo {
    private String name;
    private String height;
    private String mass;
    private String hair_color;
    private String skin_color;
    private String eye_color;
    private String birth_year;
    private String gender;
    private String homeworld;
    private List<String> films;
    private List<String> species;
    private List<String> vehicles;
    private List<String> starships;


    @Override
    public String toString() {
        return "*** Star Wars character info ***" +
                "\nName: " + name +
                "\nHeight: " + height +
                "\nMass: " + mass +
                "\nHair color: " + hair_color +
                "\nSkin color: " + skin_color +
                "\nEye color: " + eye_color +
                "\nBirth year: " + birth_year +
                "\nGender: " + gender+
//                "\nhomeworld: " + homeworld +
                "\nFilms: " + films+
                "\n";
//                "\nspecies: " + species +
//                "\nvehicles: " + vehicles +
//                "\nstarships: " + starships;
    }
}
